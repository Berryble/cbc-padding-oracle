#  For best results, use scientific python mode
import http.client

import requests as requests
from Crypto.Util.Padding import pad

URL = "127.0.0.1:5000"


def pretty_print_token(original_string):
    for idx, c in enumerate(original_string):
        if idx % 2 == 0 and idx != 0:
            print(":", end="")
        if idx % 16 == 0 and idx != 0:
            print("  ", end="")
        print(c, end="")
    print()


def ask_oracle(modified_cookie):
    cookies = {"authtoken": modified_cookie}
    return requests.get(f'http://{URL}/quote/', cookies=cookies)


def replace_char(original_string, index, character):
    index = index * 2
    return original_string[0:index] + character + original_string[index + len(character):]


def get_plain_text(byte, cook, padding):
    cb = cook[byte * 2:byte * 2 + 2]
    for i in range(256):
        hexa = hex(i)[2:] if i > 15 else f"0{hex(i)[2:]}"
        mod_cook = replace_char(cook, byte, hexa)
        byte_index = byte * 2
        cookie_to_send = (mod_cook[:byte_index] + hexa + mod_cook[byte_index + 2:64])
        rq = ask_oracle(cookie_to_send)
        # pretty_print_token(cookie_to_send)
        if "quote" in str(rq.content) or "utf" in str(rq.content):
            intermediate_text = int(hexa, 16) ^ padding
            plain_text = intermediate_text ^ int(cb, 16)
            if padding == 1:
                len_check = modify_original_cipher(cookie_to_send, intermediate_text, byte, padding + 1)
                try:
                    get_plain_text(byte - 1, len_check, padding + 1)
                    return intermediate_text, plain_text
                except RuntimeError:
                    continue
            else:
                return intermediate_text, plain_text
    raise RuntimeError("You win this time :^(")


def modify_original_cipher(modified_cookie, intermediate, index, padding):
    byte_index = index * 2
    xor_val = padding ^ intermediate
    last_bit = hex(xor_val)[2:] if xor_val > 15 else f"0{hex(xor_val)[2:]}"
    return modified_cookie[:byte_index] + last_bit + modified_cookie[byte_index + 2:64]


def decrypt_block(block_cookie):
    intermediate_bytes = list()
    plain_bytes = list()
    for i in range(15, -1, -1):
        padding = 15 - i + 1
        for j in range(15, i, -1):
            block_cookie = modify_original_cipher(block_cookie, intermediate_bytes[15 - j], j, padding)

        intermediate, plain = get_plain_text(i, block_cookie, padding)

        intermediate_bytes.append(intermediate)
        plain_bytes.append(plain)

    return plain_bytes, intermediate_bytes


def decrypt_text(str, block_size, block_count):
    plain_bytes = list()
    intermediate_bytes = list()
    for i in range(block_count - 1):
        ps, its = decrypt_block(str[block_size * i:(block_size * 2) + block_size * i])
        plain_bytes += ps[::-1]
        intermediate_bytes += its
    return plain_bytes, intermediate_bytes


# %% CONNECTION CONFIGURATION


connection = http.client.HTTPConnection(URL)
connection.request('GET', '/')
response = connection.getresponse()
cookie = response.getheader("Set-Cookie").split('=')[1].split(';')[0]

# %% DECRYPTION

BLOCK_SIZE = 32
block_count = int(len(cookie) / BLOCK_SIZE)
pbs, ibs = decrypt_text(cookie, BLOCK_SIZE, block_count)
reverse_ibs = ibs[::-1]

print(bytes.fromhex(''.join(hex(i)[2:] if i > 15 else f"0{hex(i)[2:]}" for i in pbs)))
print("Get haxormaned (^:")

# %% ENCRYPTION

attack = pad("<redacted> plain CBC is not secure!".encode(), 16).hex()
INTERMEDIATE_LENGTH = 16
fake_cookie = cookie

for i in range(0, block_count - 1):
    j = block_count - 1 - i
    intermediate = ''.join([hex(i)[2:] if i > 15 else f"0{hex(i)[2:]}" for i in reverse_ibs[i * INTERMEDIATE_LENGTH:(i + 1) * INTERMEDIATE_LENGTH]])
    cipher = hex(int(intermediate, 16) ^ int(attack[(j - 1) * BLOCK_SIZE:BLOCK_SIZE * j], 16))[2:]
    cipher = f"0{cipher}" if len(cipher) == BLOCK_SIZE - 1 else cipher
    fake_cookie = fake_cookie[:(j - 1) * BLOCK_SIZE] + cipher + fake_cookie[BLOCK_SIZE * j:]
    pbs, ibs = decrypt_text(fake_cookie, BLOCK_SIZE, block_count)
    reverse_ibs = ibs[::-1]
    print(ask_oracle(fake_cookie).content)
